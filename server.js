const express = require("express");
const bodyparser = require("body-parser");
const cors = require("cors");

const db = require("./app/model");
const auth_ROUTER = require("./app/router/auth.route");
const user_ROUTER = require("./app/router/user.route");

const PORT = process.env.PORT || 5000;

const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

app.use(cors());
// app.use(express.static("../images/posts"));
app.use("/images/posts", express.static("./images/posts"));

app.use("/auth", auth_ROUTER);
app.use("/user", user_ROUTER);

app.use((req, res, next) => {
  let error = new Error("Not Found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  console.log(JSON.stringify(error));
  let status = error.status ? error.status : 500;

  res
    .status(status)
    .json({
      status: "Failed",
      message: error.message,
    })
    .end();
});

db.sequelize
  .sync()
  .then(() => {
    console.log(`Connected to ${process.env.DB_NAME}`);
  })
  .catch((err) => {
    console.log(err);
  });
app.listen(PORT, () => {
  console.log(`Server Started On ${PORT}`);
});
