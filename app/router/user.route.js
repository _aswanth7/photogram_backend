const multer = require("multer");
const {
  addPost,
  getPosts,
  getUserPosts,
} = require("../controller/user_post.controller");
const {
  addFriend,
  removeFriend,
  friendCheck,
} = require("../controller/user_social.controller");
const user_ROUTER = require("express").Router();
const path = require("path");
const { getAllUser } = require("../controller/user_account.controller");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./images/posts");
  },
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

const postImage = multer({ storage });

user_ROUTER.post("/addfriend", addFriend);

user_ROUTER.post("/removefriend", removeFriend);
user_ROUTER.post("/addpost", postImage.single("image"), addPost);
user_ROUTER.get("/getposts", getPosts);
user_ROUTER.get("/getuserposts", getUserPosts);
user_ROUTER.post("/isfriend", friendCheck);
user_ROUTER.get("/getalluser", getAllUser);

module.exports = user_ROUTER;
