const { Signup, Login } = require("../controller/user_account.controller");

const auth_ROUTER = require("express").Router();

auth_ROUTER.get("/", (req, res) => {
  res.send("auth");
});
auth_ROUTER.post("/login", Login);

auth_ROUTER.post("/signup", Signup);

module.exports = auth_ROUTER;
