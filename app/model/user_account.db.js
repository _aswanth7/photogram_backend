module.exports = (Sequelize, sequelize) => {
  return sequelize.define("user_account", {
    username: {
      type: Sequelize.STRING,
      unique: true,
      primaryKey: true,
    },
    user_fullname: {
      type: Sequelize.STRING,
    },
    user_mail: {
      type: Sequelize.STRING,
    },

    user_phone: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
    user_bio: {
      type: Sequelize.TEXT,
      defaultValue: null,
    },
    user_profile: {
      type: Sequelize.STRING,
      defaultValue: null,
    },
    user_verified: {
      type: Sequelize.BOOLEAN,
      defaultValue: 0,
    },
  });
};
