const Sequelize = require("sequelize");

const config = require("../config/config");

const sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    dialect: config.DB_DIALECT,
  }
);

const db = {};
//all the post made by friends of  a user
//friend
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user_account = require("./user_account.db")(Sequelize, sequelize);

db.user_social = require("./user_social.db")(Sequelize, sequelize);
db.user_post = require("./user_post.db")(Sequelize, sequelize);

//     const City = sequelize.define('city', { countryCode: Sequelize.STRING });
// const Country = sequelize.define('country', { isoCode: Sequelize.STRING });

// // Here we can connect countries and cities base on country code
// Country.hasMany(City, {foreignKey: 'countryCode', sourceKey: 'isoCode'});
// City.belongsTo(Country, {foreignKey: 'countryCode', targetKey: 'isoCode'});

db.user_account.hasMany(db.user_social, {
  foreignKey: "username",
});
db.user_social.belongsTo(db.user_account, {
  foreignKey: "friend",
});

db.user_account.hasMany(db.user_post, {
  foreignKey: "username",
});
db.user_post.belongsTo(db.user_account, {
  foreignKey: "username",
});

module.exports = db;
