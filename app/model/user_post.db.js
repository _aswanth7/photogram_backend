module.exports = (Sequelize, sequelize) => {
  return sequelize.define("user_post", {
    post_id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    post_image: {
      type: Sequelize.STRING,
    },
    post_caption: {
      type: Sequelize.STRING,
    },
    username: {
      type: Sequelize.STRING,
    },
  });
};
