module.exports = (Sequelize, sequelize) => {
  return sequelize.define("user_social", {
    username: {
      type: Sequelize.STRING,
      //   references: "user_account",
      //   referencesKey: "username",
    },
    friend: {
      type: Sequelize.STRING,
    },
  });
};
