const jwt = require("jsonwebtoken");
const config = require("../config/config");
const { user_post } = require("../model/index");

const db = require("../model/index");

const OP = db.Sequelize.Op;
const UserPost = db.user_post;
const UserSocial = db.user_social;
const UserAccount = db.user_account;

const addPost = async (req, res) => {
  const token = req.headers["access-token"];
  let { username, caption } = req.body;
  console.log(req.file);
  try {
    jwt.verify(token, config.JWT_SECRET);

    result = await UserPost.create({
      username,
      post_image: req.file.path,
      post_caption: caption,
    });
    res
      .status(200)
      .json({
        status: "success",
        message: "posted successfully",
      })
      .end();
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};

const getPosts = async (req, res) => {
  try {
    let friendsList = [];
    const token = req.headers["access-token"];
    const username = jwt.verify(token, config.JWT_SECRET);

    var condition = username ? { username: { [OP.like]: username } } : null;
    //find friends
    let result = await UserSocial.findAll({
      where: condition,
    });

    result.map((data) => {
      friendsList.push(data.friend);
    });
    //get post of friends
    let userAccount_with_post = await UserAccount.findAll({
      where: {
        username: {
          [OP.in]: friendsList,
        },
      },
      include: ["user_posts"],
      order: [["createdAt", "desc"]],
    });

    //get post and send it

    let user_post = [];
    userAccount_with_post.map((user) => {
      user["user_posts"].map((post) => {
        user_post.push(post);
      });
    });
    res.status(200).json(user_post).end;
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};

const getUserPosts = async (req, res) => {
  try {
    const token = req.headers["access-token"];
    const username = req.query.username;
    jwt.verify(token, config.JWT_SECRET);
    console.log(username, "username");
    const posts = await UserPost.findAll({
      where: {
        username: {
          [OP.like]: [username],
        },
      },
      order: [["createdAt", "desc"]],
    });
    console.log(posts, "posts");
    res.status(200).json(posts).end;
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};

module.exports = { addPost, getPosts, getUserPosts };
