const jwt = require("jsonwebtoken");
const config = require("../config/config");

const db = require("../model/index");

const OP = db.Sequelize.Op;
const UserSocial = db.user_social;

const addFriend = async (req, res) => {
  let { username, friend } = req.body;

  try {
    const token = req.headers["access-token"];
    let isValid = jwt.verify(token, config.JWT_SECRET);
    console.log(isValid);

    result = await UserSocial.create({ username, friend });
    res
      .status(200)
      .json({
        status: "success",
        username,
        friend,
      })
      .end();
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};

const removeFriend = async (req, res) => {
  let { username, friend } = req.body;

  try {
    const token = req.headers["access-token"];
    let isValid = jwt.verify(token, config.JWT_SECRET);

    result = await UserSocial.destroy({ where: { username, friend } });
    res
      .status(200)
      .json({
        status: "success",
        message: "successfully UnFollowed",
      })
      .end();
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};

const friendCheck = async (req, res) => {
  let { username, friend } = req.body;

  try {
    const token = req.headers["access-token"];
    let isValid = jwt.verify(token, config.JWT_SECRET);
    console.log(isValid);

    result = await UserSocial.findOne({
      where: {
        [OP.and]: [{ username: username }, { friend: friend }],
      },
    });
    res
      .status(200)
      .json({
        status: "success",
        result: result !== null ? true : false,
      })
      .end();
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};

module.exports = { addFriend, removeFriend, friendCheck };
