const jwt = require("jsonwebtoken");
const config = require("../config/config");

const db = require("../model/index");

const OP = db.Sequelize.Op;
const UserAccount = db.user_account;

const createJWTToken = (username) => {
  return jwt.sign(username, config.JWT_SECRET);
};

const Signup = async (req, res) => {
  try {
    let { username, fullname, phone, email, password } = req.body;
    let user = await UserAccount.findByPk(username);
    if (user === null) {
      let userDate = {
        username,
        user_fullname: fullname,
        user_mail: email,
        user_phone: phone,
        password,
      };
      let result = await UserAccount.create(userDate);

      res.status(200).json({
        status: "success",
        username,
      });
    } else {
      res
        .status(409)
        .json({
          status: "Failed",
          message: "Username already exist",
        })
        .end();
    }
  } catch (error) {
    res
      .status(500)
      .send({
        status: "Failed",
        message: err.message,
      })
      .end();
  }
};

const Login = async (req, res) => {
  try {
    let { username, password } = req.body;
    let user = await UserAccount.findByPk(username);
    if (user === null) {
      res
        .status(403)
        .json({
          status: "Failed",
          message: "User Does not Exist",
        })
        .end();
    } else {
      if (user.password === password) {
        const jwtToken = createJWTToken(username);
        res.status(200).json({
          status: "success",
          "secret-token": jwtToken,
          username,
        });
      } else {
        res
          .status(403)
          .json({
            status: "Failed",
            message: "Invalid Credential",
          })
          .end();
      }
    }
  } catch (error) {
    res
      .status(500)
      .send({
        status: "Failed",
        message: err.message,
      })
      .end();
  }
};

const getAllUser = async (req, res) => {
  try {
    const name = req.query.name;
    const token = req.headers["access-token"];
    const username = jwt.verify(token, config.JWT_SECRET);

    const posts = await UserAccount.findAll({
      where: {
        username: {
          [OP.like]: `%${name}%`,
        },
      },
    });

    res.status(200).json(posts).end;
  } catch (error) {
    console.log(error);
    let code = error.name === "JsonWebTokenError" ? 401 : 500;
    res
      .status(code)
      .send({
        status: "Failed",
        message: error.message,
      })
      .end();
  }
};
module.exports = { Signup, Login, getAllUser };
